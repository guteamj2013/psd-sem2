package cluster.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import cluster.ClusterJob;
import cluster.Dashboard;

public class AddJobToClusterTest {
	
	private Dashboard dashboard;
	
	@Before
	public void setUp(){
		//Given a cluster
		dashboard = new Dashboard();
	}
	
	@Test
	public void submitAJob() {
		
		//When 1 experiment is submitted
		ClusterJob clusterJob = new ClusterJob ();
		
		Integer before = dashboard.getJobCount();
		dashboard.submitJob(clusterJob);
		Integer after = dashboard.getJobCount();

		//Then the experiment is started
		assertTrue(dashboard.isRunning(clusterJob));
		
		//And the experiment is added to the dashboard
		Integer expected = 1;
		Integer actual = after - before;
		
		assertEquals("", expected, actual);
	}
}
