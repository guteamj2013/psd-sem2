package cluster;

import java.util.HashSet;
import java.util.Set;

public class Dashboard {
	
	private Set<ClusterJob> clusterJobs = 
		new HashSet<ClusterJob>();

	public Integer getJobCount() {
		return clusterJobs.size();
	}

	public void submitJob(ClusterJob clusterJob) {
		clusterJobs.add(clusterJob);
	}

	public Boolean isRunning(ClusterJob clusterJob) {
		return clusterJobs.contains(clusterJobs);
	}

}
