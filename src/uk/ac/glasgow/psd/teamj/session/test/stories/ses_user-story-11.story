Narrative: Student booking a slot
	As a student
	I want to book a timetable slot for each session of my course,
	So that I can take the course.
	
Scenario:
Given a valid slot group A by student with id 1111111
When trying to book a timetable slot
Then the student is added to the slot
	
Scenario:
Given a valid slot group A by student with id 1111111
When trying to book a timetable slot and the student already has booked the slot
Then the student is not added to slot again
		
Scenario: 
Given an invalid slot ID XSFDFSGFGD not in the database
When trying to book a timetable slot
Then the student will not be added to the slot
	
Scenario:
Given a valid slot group A by tutor with id 3333333
When trying to book a timetable slot
Then the system will not add the user to the slot as the user has to be a student.
