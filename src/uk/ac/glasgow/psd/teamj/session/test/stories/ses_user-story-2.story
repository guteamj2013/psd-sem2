Narrative: Lecturer adding a session to a course
As a lecturer,
I want to add a session to a course,
So that timetable slots can be identified.
	
Scenario:
Given a session JP2Lab1 by admin 0000000
When trying to add a session to a course
Then the session is added to the database
	
Scenario:
Given a session JP2Lab1 by admin 0000000
When trying to add a session that is already in the DB to a course
Then the session is not added
		
Scenario: 
Given a session null by admin 0000000
When trying to add a session to a course
Then the system will not add the course to the DB.
	
Scenario:
Given a session JP2Lab1 by student 111111
When trying to add a session to a course
Then the system will not add the course as the user is not privileged.
