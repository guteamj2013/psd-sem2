Narrative:
In order for students to complete the course
As an administrator
I want to check that there are not timetable clashes between courses

Scenario:
Given an added session slot group A by admin 0000000
When I check a slot against another course that clashes with 1 slot
Then the slot is returned

Scenario:
Given an added session slot group A by admin 0000000
When I check a slot against another course that does not clash with any slots
Then no slot is returned

Scenario: security
Given an added session slot group A by student 111111
When I check a slot against another course
Then no check will be made