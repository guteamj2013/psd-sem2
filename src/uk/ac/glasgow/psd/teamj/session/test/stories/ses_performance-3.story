Narrative: The system shall support at least 20 different timetable slots per session
As an admin or lecturer
I want to offer at least 20 timetable slots per session
so my students are able to choose a slot that suits them

Scenario:
Given a new slot for a session
When session has less than 20 slots assigned to it already
Then the slot should be assigned to the session                                                                                                        

Scenario: 
Given a new slot for a session
When session have 20 or more sessions assigned to it
Then the slot may be assigned to the session or assignment may fail