Narrative:
The system shall be implemented as one or more OSGi bundles.

Scenario:
OSGi start and stop methods execute properly
Given a current system
When functionality must be launched
Then it can be done with start method