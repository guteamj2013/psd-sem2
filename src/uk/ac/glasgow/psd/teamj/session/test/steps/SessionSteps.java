package uk.ac.glasgow.psd.teamj.session.test.steps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.isOneOf;
import static org.hamcrest.Matchers.notNullValue;

import java.util.ArrayList;
import java.util.GregorianCalendar;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.osgi.framework.BundleContext;
import org.springframework.osgi.mock.MockBundleContext;

import uk.ac.glasgow.psd.teamj.db.Course;
import uk.ac.glasgow.psd.teamj.db.Database;
import uk.ac.glasgow.psd.teamj.db.Slot;
import uk.ac.glasgow.psd.teamj.db.User;
import uk.ac.glasgow.psd.teamj.session.Activator;
import uk.ac.glasgow.psd.teamj.session.PermissionDeniedException;
import uk.ac.glasgow.psd.teamj.session.Session;
import uk.ac.glasgow.psd.teamj.session.SessionInterface;

public class SessionSteps {

	private Database db = Database.getDB();
	private SessionInterface sessionComponent = new Session(db);
	private String sessionID;
	private boolean compulsary;
	private String frequency;
	private String courseID;
	private String roomNum;
	private String building;
	private String slotID;
    private String course2ID;
    private Slot slotCheck;
	private User u;
	private Activator a;
	private BundleContext bundleContext;

	/* User Story 2 Steps */

	@Given("a session $sID by $type $uID")
	public void validSessionAuth(String sID, String type, String uID) throws Exception {
		if(type.equals("admin")){
		u = db.getAdmin(uID);}
		else if(type.equals("lecturer")){
			u = db.getLecturer(uID);}
		else if(type.equals("student")){
			u = db.getStudent(uID);}
		else if(type.equals("tutor")){
			u = db.getTutor(uID);}
		else{ throw new Exception("Invalid Type");}
		sessionID = sID;
		if (sID.equals("null")) sessionID = null;
		compulsary = true;
		frequency = "weekly";
		courseID = "COMPSCI 2001";
	}

	@When("trying to add a session to a course")
	public void addingSession() {

	}
	@When("trying to add a session that is already in the DB to a course")
	public void addingExistingSession() {

	}

	@Then("the session is added to the database")
	public void isAdded() throws PermissionDeniedException {
		boolean success = sessionComponent.addSession(u, courseID, sessionID,
				frequency, compulsary);
		assertThat(success, equalTo(true));
	}

	@Then("the session is not added")
	public void isntAdded() throws PermissionDeniedException {
		boolean success = sessionComponent.addSession(u, courseID, sessionID,
				frequency, compulsary);
		assertThat(success, equalTo(false));
	}


	@Then("the system will not add the course to the DB.")
	public void doesNotAdd() throws PermissionDeniedException {
		boolean success = sessionComponent.addSession(u, courseID, sessionID,
				frequency, compulsary);
		assertThat(success, equalTo(false));
	}

	@Then("the system will not add the course as the user is not privileged.")
	public void courseNotAddedExceptionThrown() {
		try {
			boolean success = sessionComponent.addSession(u, courseID,
					sessionID, frequency, compulsary);
			// make the test fail
			assertThat(false, equalTo(true));
		} catch (Exception e) {
			assertThat(true, equalTo(true));
		}
	}

	/* User Story 4 Steps */
	@Given("a frequency $freq")
	public void aFrequency(String freq) {
		frequency = freq;
	}

	@When("creating a new session $sID")
	public void newSessionDetails(String sID) {
		sessionID = sID;
		courseID = "COMPSCI 2001";
		compulsary = false;
		u= db.getAdmin("0000000");
	}

	@Then("the session will be created with the given frequency")
	public void isAddedwithFrequency() throws PermissionDeniedException {
		boolean success = sessionComponent.addSession(u, courseID, sessionID,
				frequency, compulsary);
		assertThat(success, equalTo(true));
	}

	/* User Story 8 Steps */
	@Given("a room number $num")
	public void newRoom(String num) {
		u = db.getAdmin("0000000");
		sessionID = "JP2Lab1";
		roomNum = num;
		building = "Boyd Orr Building";
		slotID = "group A";

	}

	@When("the slot does not have a room assigned to it")
	public void noSlotAssigned() {
		db.addSlot(null, null, slotID, 100, new GregorianCalendar(),new GregorianCalendar());
	}

	@Then("the room number should be assigned to the timetable slot")
	public void assignSlot() throws PermissionDeniedException {
		boolean success = sessionComponent.assignRoom(u, courseID, sessionID,
				slotID, building, roomNum);
		assertThat(success, equalTo(true));
	}

	@When("the slot already has a room assigned to it")
	public void roomAssigned() {
		// Is the case because of the last scenario
	}

	@Then("the new room number should replace the previous number assigned to the slot.")
	public void replaceRoom() throws PermissionDeniedException {
		boolean success = sessionComponent.assignRoom(u, courseID, sessionID,
				slotID, building, roomNum);
		assertThat(success, equalTo(true));
	}

    /* User Story 9 steps */
    @Given("an added session slot $slID by $type $uID")
	public void aAddedSlot(String slID, String type, String uID) throws Exception {
    	
    		if(type.equals("admin")){
    		u = db.getAdmin(uID);}
    		else if(type.equals("lecturer")){
    			u = db.getLecturer(uID);}
    		else if(type.equals("student")){
    			u = db.getStudent(uID);}
    		else if(type.equals("tutor")){
    			u = db.getTutor(uID);}
    		else{ throw new Exception("Invalid Type");}
    		courseID = "COMPSCI 2001";
    		course2ID = "COMPSCI 2002";
    		sessionID = "JP2Lab1"; 
    		roomNum = "507D";
			building = "Boyd Orr Building";
            slotID = slID;
            db.addSlot(roomNum, building, slotID, 150, new GregorianCalendar(2013,9,17,10,00),new GregorianCalendar(2013,9,17,11,00));
               
                
	}
  
    
        
    @When("I check a slot against another course that clashes with 1 slot")
	public void CheckWithClash() {
        	
		
	}
        
    @Then("the slot is returned")
	public void SlotReturned() throws PermissionDeniedException {
            boolean success = sessionComponent.checkClashes(u, courseID, sessionID, slotID, course2ID)!=null;
            assertThat(success, equalTo(true));
		
	}
         @When("I check a slot against another course that does not clash with any slots")
     	public void CheckWithNoClash() {

     		
     	}      
 

        
        @Then("no slot is returned")
	public void NoSlotReturned() throws PermissionDeniedException {
        	boolean success = sessionComponent.checkClashes(u, courseID, sessionID, slotID, course2ID)!=null;
            assertThat(success, equalTo(false));
		
	}
    
        @When("I check a slot against another course")
		public void checkSlot() throws PermissionDeniedException {
        		//simple check no conditions
			
		}    
         

	
	@Then("no check will be made")
	public void checkClashExceptionThrown() {
		try {
			boolean success = sessionComponent.checkClashes(u, courseID, sessionID, slotID, course2ID) !=  null;
			// make the test fail
			assertThat(false, equalTo(true));
		} catch (Exception e) {
			assertThat(true, equalTo(true));
		}
	}
        
	  
	
	   
        
	/* User Story 11 steps */

	@Given("a valid slot $slID by $type with id $uID")
	public void validSlotNotBooked( String slID, String type, String uID) throws Exception {
		sessionID = "JP2Lab1";
		slotID = slID;
		courseID = "COMPSCI 2001";
		if(type.equals("admin")){
			u = db.getAdmin(uID);}
			else if(type.equals("lecturer")){
				u = db.getLecturer(uID);}
			else if(type.equals("student")){
				u = db.getStudent(uID);}
			else if(type.equals("tutor")){
				u = db.getTutor(uID);}
			else{ throw new Exception("Invalid Type");}
	}

	@When("trying to book a timetable slot")
	public void tryingtoBookSlot() {

	}
	@When("trying to book a timetable slot and the student already has booked the slot")
	public void tryingtoBookSlotAlreadyBooked() {

	}

	@Then("the student is added to the slot")
	public void addedToSlot() {
		boolean success = sessionComponent.bookSlot(u, courseID, sessionID,
				slotID);
		assertThat(success, equalTo(true));
	}

	@Then("the student is not added to slot again")
	public void noAddedtoSlot() {
		boolean success = sessionComponent.bookSlot(u, courseID, sessionID,
				slotID);
		assertThat(success, equalTo(false));
	}

	@Given("an invalid slot ID $slID not in the database")
	public void invalidSlotID(String slID) {
		slotID = slID;
		courseID = "COMPSCI 2001";
		sessionID = "JP2Lab1";
		u = db.getAdmin("0000000");
	}

	@Then("the student will not be added to the slot")
	public void studentNotAdded() {
		boolean success = sessionComponent.bookSlot(u, courseID, sessionID,
				slotID);
		assertThat(success, equalTo(false));
	}

	@Given("a valid slot by a lecturer or tutor")
	public void validSlotNotAuthd() {
		u = db.getAdmin("0000000");
		slotID = "JP2Lab1";
		courseID = "COMPSCI 2001";
		sessionID = "JP2Lab1";
	}

	@Then("the system will not add the user to the slot as the user has to be a student.")
	public void userNotAdded() {
		boolean success = sessionComponent.bookSlot(u, courseID, sessionID,
				slotID);
		assertThat(success, equalTo(false));
	}

	/* User Story 14 Steps */

	@Given("a valid UoG course $course and session ID $session")
	public void validDetails(String course, String session) {
		sessionID = session;
		courseID = course;
	}

	@When("trying to check the slot details of a session")
	public void checkingDetails() {

	}

	@Then("the session details are retrieved from the database")
	public void detailsRetrieved() {
		ArrayList<String> details = sessionComponent.slotDetails(courseID,
				sessionID);
		assertThat(details, notNullValue());
	}

	@Given("an invalid session ID $session for valid course $course")
	public void invalidID(String session, String course) {
		sessionID = session;
		courseID = course;
	}

	@Then("the details will not be retrieved from the database")
	public void detailsNotRetrieved() {
		ArrayList<String> details = sessionComponent.slotDetails(courseID,
				sessionID);
		assertThat(details, equalTo(null));
	}

	/* performance 1 steps */

	@Given("a new session $session for a course $course with ID $cid")
	public void newCourseSession(String session, String course, String cid) {
		db.addCourse(course, cid);
		courseID = cid;
		u = db.getAdmin("0000000");
		sessionID = session;
		frequency = "once";
		compulsary = true;
	}

	@When("the course has less than 10 sessions assigned to it already")
	public void lessThan10() {
		// already the case
	}

	@Then("the session should be assigned to the course")
	public void sessionAssigned() throws PermissionDeniedException {
		for (int i = 1; i <= 10; i++) {
			boolean success = sessionComponent.addSession(u, courseID,
					sessionID + i, frequency, compulsary);
			assertThat(success, equalTo(true));
		}
	}

	@When("the course has 10 or more sessions assigned to it already")
	public void tenorMore() {
		// already the case from last scenario
	}

	@Then("the session may be assigned to the course or assignment may fail")
	public void mayOrmayNotFail() throws PermissionDeniedException {
		boolean success = sessionComponent.addSession(u, courseID, sessionID
				+ "11", frequency, compulsary);
		assertThat(success, isOneOf(true, false));
	}

	/* performance 3 steps */

	@Given("a new slot for a session")
	public void newSessionSlot() {
		u = db.getAdmin("0000000");
		slotID = "group ";
		roomNum = "302";
		building = "Boyd Orr Building";
	}

	@When("session has less than 20 slots assigned to it already")
	public void lessthan20slots() {

	}

	@Then("the slot should be assigned to the session")
	public void slotAssigned() {
		uk.ac.glasgow.psd.teamj.db.Session dbsession = db
				.getSession("Lecture 11");
		for (int i = 1; i <= 10; i++) {
			db.addSlot(roomNum, building, slotID + i, 100, new GregorianCalendar(),new GregorianCalendar());
			Slot sl = db.getSlot(slotID + i);
			boolean success = dbsession.addSlot(sl);
			assertThat(success, equalTo(true));
		}
	}

	@When("session have 20 or more sessions assigned to it")
	public void twentyorMoreSessions() {

	}

	@Then("the slot may be assigned to the session or assignment may fail")
	public void mayAssignSession() {
		uk.ac.glasgow.psd.teamj.db.Session dbsession = db.getSession("Lecture 11");
		db.addSlot(roomNum, building, slotID + 11, 100, new GregorianCalendar(),new GregorianCalendar());
		Slot sl = db.getSlot(slotID + 11);
		boolean success = dbsession.addSlot(sl);
		assertThat(success, isOneOf(true, false));
	}

	/* interopability 0 scenarios */

	@Given("a current system")
	public void newSystem() {
		a = new Activator();
	}

	@When("functionality must be launched")
	public void launchFunctionality() {

	}

	@Then("it can be done with start method")
	public void start() {
		try {
			bundleContext = new MockBundleContext();
			a.start(bundleContext);
			assertThat(true, equalTo(true));
		} catch (Exception e) {
			assertThat(false, equalTo(false));
		}
	}

}
