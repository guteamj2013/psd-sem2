Narrative: 
As a lecturer, I want to specify that a session is a one off, or recurs weekly or fortnightly, So that
I don't have to create a large number of sessions.

Scenario: 
Given a frequency fortnightly
When creating a new session JPLab3
Then the session will be created with the given frequency
