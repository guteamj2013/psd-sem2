Narrative:
The system shall support at least 10 different session types per course.


Scenario:
Given a new session Lecture  for a course Networked Systems 3 with ID COMPSCI 3004
When the course has less than 10 sessions assigned to it already
Then the session should be assigned to the course                                                                                                     



Scenario: 
Given a new session Lecture  for a course Networked Systems 3 with ID COMPSCI 3004
When the course has 10 or more sessions assigned to it already
Then the session may be assigned to the course or assignment may fail

                                                                                                    

