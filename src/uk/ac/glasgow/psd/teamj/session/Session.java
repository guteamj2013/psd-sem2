package uk.ac.glasgow.psd.teamj.session;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingDeque;

import uk.ac.glasgow.psd.teamj.db.Course;
import uk.ac.glasgow.psd.teamj.db.Database;
import uk.ac.glasgow.psd.teamj.db.Slot;
import uk.ac.glasgow.psd.teamj.db.User;

public class Session implements SessionInterface {

	private Database db;

	public Session(Database db) {
		this.db = db;
	}

	public boolean addSession(User u, String courseid, String sessionid,
			String frequency, boolean compulsary)
			throws PermissionDeniedException {

		if (u.getType().equals("student") || u.getType().equals("tutor")) {
			throw new PermissionDeniedException();

		}
		db.addSession(courseid, sessionid, frequency, compulsary);
		return true;
	}

	public boolean assignRoom(User u, String courseid, String sessionid,
			String slotid, String Building, String Room)
			throws PermissionDeniedException {
		if (!u.getType().equals("administrator"))
			throw new PermissionDeniedException();
		Slot slot = db.getSlot(slotid);
		slot.setRoom(Room);
		slot.setBuilding(Building);

		return true; // assigning a room failed

	}

	public boolean bookSlot(User u, String courseid, String sessionid,
			String slotid) {
		/*
		 * get the slot from the Database, check if it is full, return false if
		 * so otherwise book slot and if necessary reduce numslots avail
		 */
		if (u.getType().equals("student")) {

			Slot slot = db.getSlot(slotid);
			if (slot == null)
				return false;
			return slot.addStudent(u.getID());
		}
		return false;
	}

	/*
	 * Checks if a slot clashes with another course's slots. If clash is found.
	 * It returns the slot that caused the clash If no clash is found it returns
	 * empty
	 */

	public synchronized Slot checkClashes(User u, String courseid,
			String sessionid, String slotid, String courseid2)
			throws PermissionDeniedException {

		if (!u.getType().equals("admin"))
			throw new PermissionDeniedException();

		else {
			Slot slot = db.getSlot(slotid);
			Course course = db.getCourse(courseid2);
			for (String sess : course.getSessions())
				for (Slot s : db.getSession(sess).getSlots())
					if (!(slot.getStartDateTime().after(s.getEndDateTime()))
							&& !(s.getStartDateTime().after(slot
									.getEndDateTime())))
						return s;
		}

		return null;
	}

	/**
	 * Return a list of all of the slot details for all of the slots for a
	 * particular session
	 */

	public ArrayList<String> slotDetails(String courseID, String sessionId) {
		uk.ac.glasgow.psd.teamj.db.Session dbsession = db.getSession(sessionId);
		if (dbsession == null)
			return null;
		ArrayList<String> result = new ArrayList<String>();
		for (Slot s : dbsession.getSlots()) {
			result.add(s.toString());
		}
		return result;
	}
	
	public boolean editExistingSession(User u,String sessionid,
			String frequency, boolean compulsary)
			throws PermissionDeniedException {
		if (u.getType().equals("student") || u.getType().equals("tutor")) {
			throw new PermissionDeniedException();
		}
		uk.ac.glasgow.psd.teamj.db.Session sess = db.getSession(sessionid);
		sess.setCompulsory(compulsary);
		sess.setFrequency(frequency);
		return true;
	}

}
