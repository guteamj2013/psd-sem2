package uk.ac.glasgow.psd.teamj.session;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.osgi.framework.ServiceReference;



import uk.ac.glasgow.psd.teamj.db.Database;

public class Activator implements BundleActivator {

	private ServiceRegistration<SessionInterface> 
		sessionRegistration;
	
	
	@Override
	public void start(BundleContext context)
		throws Exception {
		
		ServiceReference<Database> serviceReference =
				context.getServiceReference(Database.class);
		
		Database db = context.getService(serviceReference);
		
		Session sessionHandler = 
			new Session(db);
		
		
		sessionRegistration = 
			context.registerService(
				SessionInterface.class, sessionHandler, null);	
	}	

	@Override
	public void stop(BundleContext context)
		throws Exception {
		
		sessionRegistration.unregister();
	}
}