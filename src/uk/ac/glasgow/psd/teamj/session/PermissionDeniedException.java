package uk.ac.glasgow.psd.teamj.session;

public class PermissionDeniedException extends Exception{
	
	public PermissionDeniedException(String s){
		super("Permission Denied!\n "+ s);
		
	}
	
	public PermissionDeniedException(){
		super("Permission Denied!");
	}
}
