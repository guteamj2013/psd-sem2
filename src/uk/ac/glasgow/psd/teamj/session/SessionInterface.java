package uk.ac.glasgow.psd.teamj.session;

import uk.ac.glasgow.psd.teamj.db.User;

import java.util.ArrayList;

import uk.ac.glasgow.psd.teamj.db.Slot;

public interface SessionInterface {
	
	/* add a session to course */
	public boolean addSession(User u, String courseid, String sessionid, String frequency, boolean  compulsary) throws PermissionDeniedException;
	
	/*assign a room to a course*/
	public boolean assignRoom(User u,String courseid, String sessionid, String slotid, String Building, String Room) throws PermissionDeniedException;
	
	/*get the slot from the Database, check if it is full, return false if so otherwise book
	 * slot and if necessary reduce numslots avail */
	public boolean bookSlot(User u, String courseid, String sessionid, String slotid);
        
        
         /*
         * Checks if a slot clashes with another course's slots.
         *  If clash is found. It returns the slot that caused the clash
         *  If no clash is found it returns empty
         */ 
        public Slot checkClashes(User u, String courseid, String sessionid,String slotid, String courseid2) throws PermissionDeniedException;;
	
	/** check user is attending all compulsory sessions
	 * for all courses they are signed up for 
	 */
	public ArrayList<String> slotDetails(String courseID, String sessionId);

	public boolean editExistingSession(User u,String sessionid,
			String frequency, boolean compulsary)
			throws PermissionDeniedException;
}
