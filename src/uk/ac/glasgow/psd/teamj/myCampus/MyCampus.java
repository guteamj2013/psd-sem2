package uk.ac.glasgow.psd.teamj.myCampus;

import java.util.ArrayList;
import java.util.HashMap;

import uk.ac.glasgow.psd.teamj.db.User;


public class MyCampus {
    
    
    
    private static HashMap<String, String> users = new HashMap<String, String>();
    private static HashMap<String, String> credentials = new HashMap<String, String>();    
    private HashMap<String, ArrayList<String>> course;
    
    
    
    MyCampus(){
        users = new HashMap<String, String>();
        credentials = new HashMap<String, String>();
        course = new HashMap<String, ArrayList<String>>();
        populate();
        
        }
    
    public String getUser(String ID){
        if (users.containsKey(ID))
            return users.get(ID);
        return null;
    }
   
    public boolean checkCred(String id, String pass){
        if (credentials.containsKey(id))
            if (credentials.get(id).equals(pass))
                return true;
        return false;
        
    }
    
    public String getPass(String id){
        return credentials.get(id);
        
    }
    
    
    public boolean checkUserID(String id){        
        return credentials.containsKey(id);
    }
    
    public boolean checkCourseID(String cID){        
        return course.containsKey(cID);
    }
    
    public ArrayList<String> getCourseInfo(String cID){
        return course.get(cID);
    }
    
    public String getCourseName(String cID){
        return course.get(cID).get(0);
    }
    
    public String getCourseAdmin(String cID){
        return course.get(cID).get(1);
    }
    
   

    private void populate() {
       credentials.put("admin", "pass");
       credentials.put("0000000", "adminpass");
       credentials.put("2222222", "lecturerpass");
       credentials.put("3333333", "tutorpass");
       credentials.put("1111111", "studentpass");
       credentials.put("5555555", "studentpass");
       credentials.put("8888888", "studentpass");
       credentials.put("9999999", "studentpass");
       credentials.put("6666666", "studentpass");
       
       users.put("admin", "admin");
       users.put("0000000", "admin");
       users.put("0000001", "admin");
       users.put("0000002", "admin");
       users.put("0000003", "admin");
       users.put("2222222", "lecturer");
       users.put("3333333", "tutor");
       users.put("1111111", "student");
       users.put("5555555", "student");
       users.put("8888888", "student");
       users.put("9999999", "student");
       users.put("6666666", "student");       
       

       ArrayList<String> templist = new ArrayList<String>();
       templist.add("Java Programming 2");
       templist.add("0000000");
       course.put("COMPSCI 2001", templist);
       templist = new ArrayList<String>();
       templist.add("Object Oriented Software Engineering 2");
       templist.add("0000001");
       course.put("COMPSCI 2002", templist);
       templist = new ArrayList<String>();
       templist.add("Advanced Programming 3");
       templist.add("0000002");
       course.put("COMPSCI 2003", templist);
       templist = new ArrayList<String>();
       templist.add("Network Systems 3");
       templist.add("0000003");
       course.put("COMPSCI 2004", templist);
       
    	}
    
    public static void addNewUser(User user, String pass){
    	credentials.put(user.getID(), pass);
    	users.put(user.getID(), user.getType());
    }
    
	
}