package uk.ac.glasgow.psd.teamj.myCampus;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;

import uk.ac.glasgow.psd.teamj.db.Database;


public class Activator implements BundleActivator {

    
	private ServiceRegistration<MyCampusConnectionInterface> 
		MyCampusRegistration;
	
	
	public void start(BundleContext context)
		throws Exception {
		
		ServiceReference<Database> serviceReference =
				context.getServiceReference(Database.class);
		
		Database db = context.getService(serviceReference);
		
		
		MyCampusConnectionInterface MChandler = 
			new MyCampusConnection(db);
		
		
		MyCampusRegistration = 
			context.registerService(
				MyCampusConnectionInterface.class, MChandler, null);	
	}	


	public void stop(BundleContext context)
		throws Exception {
		
		MyCampusRegistration.unregister();
	}
}
