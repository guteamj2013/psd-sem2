package uk.ac.glasgow.psd.teamj.myCampus;

import uk.ac.glasgow.psd.teamj.session.PermissionDeniedException;
import uk.ac.glasgow.psd.teamj.db.*;

public class MyCampusConnection implements MyCampusConnectionInterface {
    
    private MyCampus mc;
    private Database db;
    
    
    public MyCampusConnection(Database db){
        
        mc = new MyCampus();
        this.db = db;
        
    }
    
    public String Authenticate(String ID, String pass){
       // System.out.println(ID+" "+pass);
        if (mc.checkCred(ID,pass))
            return mc.getUser(ID);
        
        return null;
    }
    
    public boolean importCourse(User u, String courseID) throws PermissionDeniedException{
        
        try{
        	if (u.getType().matches("student|tutor")) {
    			throw new PermissionDeniedException();
    		} else {
    			if (courseID.equalsIgnoreCase("test")) return false;
    			String courseName = mc.getCourseName(courseID);
    			String admin = mc.getCourseAdmin(courseID);
        
    			db.addCourse(courseID, courseName, admin);
        
    			return true;
    		}
        }catch(Exception e){
            e.printStackTrace();
            System.out.println("Error importing");
            return false;
        }
    }
    
    
    
    
}
