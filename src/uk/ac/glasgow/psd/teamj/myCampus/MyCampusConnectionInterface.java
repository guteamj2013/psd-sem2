package uk.ac.glasgow.psd.teamj.myCampus;

import uk.ac.glasgow.psd.teamj.session.PermissionDeniedException;
import uk.ac.glasgow.psd.teamj.db.*;

public interface MyCampusConnectionInterface {

    
    public String Authenticate(String ID, String pass);
    
    public boolean importCourse(User u, String courseID) throws PermissionDeniedException;
}
