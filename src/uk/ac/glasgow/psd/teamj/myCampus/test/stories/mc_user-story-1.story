Narrative: Lecturer Imports Course
As a lecturer,
I want to import a MyCampus course,
So that teaching sessions can be identified.

Scenario: 
Given a course TEST COURSE, not-in the system by a lecturer
When trying to add this course
Then the course TEST COURSE exists in the database
And sessions/students/lecturers are able to be added

Scenario: 
Given a course TEST COURSE, not-in the system by a administrator
When trying to add this course
Then the course TEST COURSE exists in the database
And sessions/students/lecturers are able to be added

Scenario: 
Given a course TEST COURSE, in the system by a lecturer
When trying to import this course
Then the system will not add the course to the DB.
And duplicate entry in DB is not created and no more than 1 TEST COURSE exists.

Scenario: 
Given a course TEST COURSE,  in the system by a administrator
When trying to import this course
Then the system will not add the course to the DB.
And duplicate entry in DB is not created and no more than 1 TEST COURSE exists..

Scenario: 
Given an invalid course by a lecturer
When trying to import a course
Then the system will not add the course to the DB.

Scenario: 
Given an invalid course by a administrator
When trying to import a course
Then the system will not add the course to the DB.
	
Scenario:
Given a course TEST COURSE, not-in the system by a student
When trying to import a course
Then the system will not add the course and throw and exception

Scenario:
Given a course TEST COURSE, not-in the system by a tutor
When trying to import a course
Then the system will not add the course and throw and exception

Scenario:
Given a course TEST COURSE, in the system by a student
When trying to import a course
Then the system will not add the course and throw and exception

Scenario:
Given a course TEST COURSE, in the system by a tutor
When trying to import a course
Then the system will not add the course and throw and exception