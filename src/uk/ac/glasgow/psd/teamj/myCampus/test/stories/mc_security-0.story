Narrative:
The system shall authenticate users via the MyCampus single sign-on service.

Scenario:
Given any valid login combination
Then the system will authenticate him via myCampus and return user ID

Scenario:
Given any invalid login combination
Then the system will authenticate him via myCampus and return null

