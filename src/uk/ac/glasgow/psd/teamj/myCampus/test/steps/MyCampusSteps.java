package uk.ac.glasgow.psd.teamj.myCampus.test.steps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import java.util.Random;
import java.util.Set;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.osgi.framework.BundleContext;
import org.springframework.osgi.mock.MockBundleContext;

import uk.ac.glasgow.psd.teamj.session.Activator;
import uk.ac.glasgow.psd.teamj.session.PermissionDeniedException;
import uk.ac.glasgow.psd.teamj.session.Session;
import uk.ac.glasgow.psd.teamj.user.UserFactory;
import uk.ac.glasgow.psd.teamj.db.Admin;
import uk.ac.glasgow.psd.teamj.db.Course;
import uk.ac.glasgow.psd.teamj.db.Database;
import uk.ac.glasgow.psd.teamj.db.Lecturer;
import uk.ac.glasgow.psd.teamj.db.Student;
import uk.ac.glasgow.psd.teamj.db.Tutor;
import uk.ac.glasgow.psd.teamj.db.User;
import uk.ac.glasgow.psd.teamj.myCampus.MyCampus;
import uk.ac.glasgow.psd.teamj.myCampus.MyCampusConnection;

public class MyCampusSteps {
	MyCampusConnection campus = new MyCampusConnection(Database.getDB());
	private UserFactory factory = new UserFactory(Database.getDB());
	private User userInstance;
	private Course c;
	private boolean excThrown = false;
	private boolean courseImported = false;

	@Given("any $validity login combination")
	public void createUser(String validity) {
		if (validity.equalsIgnoreCase("valid")) {
			userInstance = new Student("test", "test", "test");
			factory.register(userInstance, "pass");
		}
	}

	@Then("the system will authenticate him via myCampus and return user ID")
	public void authenticateUser() {
		assertThat(campus.Authenticate("test", "pass"), equalTo(userInstance.getType()));
	}

	@Then("the system will authenticate him via myCampus and return null")
	public void attemptLogin() {
		assertThat(campus.Authenticate("random", "nonsense"), equalTo(null));
	}

	@Given("course $isInSystem in the system by a lecturer or administrator")
	public void createNewCourse(String isInSystem) {
		if (isInSystem.equalsIgnoreCase("in")) {
			c = Database.getDB().getCourse("COMPSCI 2001");
		}
		// u = new Admin("newAdmin","newAdmin");
	}

	@When("trying to add a course")
	public void addingCourse() {
		// assertThat(null, equalTo(campus.importCourse(, courseID)));
	}

	@Then("the course is added to the database")
	public void checkAdded() {
		assertThat("newName", equalTo(Database.getDB().getCourse("newID")
				.getName()));
	}

	// Interoperability 0 Steps

	private Activator a;

	@Given("a current system")
	public void newSystem() {
		a = new Activator();
	}

	@When("functionality must be launched")
	public void launchFunctionality() {
		// I want to add functionality
	}

	private BundleContext bundleContext;

	@Then("it can be done with start method")
	public void start() {
		try {
			bundleContext = new MockBundleContext();
			a.start(bundleContext);
			assertThat(true, equalTo(true));
		} catch (Exception e) {
			assertThat(false, equalTo(false));
		}
	}

	// ************************* user story 1 *******************************//
	
	
	// the following 2 methods are borrowed from user steps, was unsure if importing and using
	// other test's methods was appropriate
	
	private void generateUser(String id, String name, String surname) {
		User temp = new Student(id, name, surname);
		Database.getDB().addStudent(name, surname, id);
		factory.register(temp, "pass");
	}

	private void createNewUser(String user) {

		if (user.equalsIgnoreCase("student")) {
			userInstance = new Student("1", "student", "student");
			Database.getDB().addStudent("test", "test", "1");
		} else if (user.equalsIgnoreCase("lecturer")) {
			userInstance = new Lecturer("1", "lecturer", "lecturer");
			Database.getDB().addLecturer("test", "test", "1");
		} else if (user.equalsIgnoreCase("administrator")) {
			userInstance = new Admin("1", "admin", "admin");
			Database.getDB().addAdmin("test", "test", "1");
		} else if (user.equalsIgnoreCase("tutor")) {
			userInstance = new Tutor("1", "tutor", "tutor");
			Database.getDB().addTutor("test", "test", "1");
		} else
			System.out.println(user);
		this.generateUser("1", "new", "new");
		factory.register(userInstance, "pass");

	}

	@Given("a course $courseName, $isInSystem by a $user")
	public void init(String courseName, String isInSystem, String user) {
		c = new Course(courseName, courseName);
		if (isInSystem.equalsIgnoreCase("in the system")) {
			if (Database.getDB().getCourse(courseName) == null)
				Database.getDB().addCourse(c.getName(), c.getCid());
			else
				assertThat(false, equalTo(true));
		}
		createNewUser(user);
	}
	
	
	@When("trying to add this course")
	public void addTheCourse(){
		Database.getDB().addCourse(c.getName(), c.getCid());
	}
	
	@Then("the course $courseName exists in the database")
	public void checkCourseExists(String courseName){
		Course temp = Database.getDB().getCourse(courseName);
		assertThat(true,equalTo(temp !=null));	// asserts that getting a course does not return a null
	}
	
	@Then("sessions/slots are able to be added")
	public void addStuff(){
		assertThat(true, equalTo(c.addSession("test session") && c.addStudent("test student")
									&& c.addLecturer("test lecturer")));
	}
	
	
	@When("trying to import this course")
	public void importCourse(){
		try {
			courseImported = campus.importCourse(userInstance, c.getCid());
		} catch (PermissionDeniedException e) {
			excThrown = true;
			courseImported = false;
		}
	}
	
	@Then ("the system will not add the course and throw and exception")
	public void checkThatExceptionThrown(){
		assertThat(true,equalTo(excThrown == true && courseImported == false));
	}
	
	@Then("adding course fails")
	public void importFailed(){
		assertThat(true, equalTo(courseImported == false));
	}
	
	@Then("duplicate entry in DB is not created and no more than 1 $couseName exists.")
	public void checkDuplicate(String courseName){
		Set<String> courses =Database.getDB().getCourses();
		courses.remove(courseName);
		assertThat(false, equalTo(courses.contains(courseName)));
	}
}
