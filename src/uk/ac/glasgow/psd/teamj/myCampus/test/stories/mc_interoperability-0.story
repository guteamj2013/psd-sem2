Narrative:
The system shall be implemented as one or more OSGi bundles.

Scenario:
Given a current system
When new functionality must be added
Then it can be done with OSGi bundles

Scenario:
Given a current system
When functionality must be launched
Then it can be done with start method
