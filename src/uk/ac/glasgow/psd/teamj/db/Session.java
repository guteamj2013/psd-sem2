package uk.ac.glasgow.psd.teamj.db;

import java.io.Serializable;
import java.util.Iterator;
import java.util.concurrent.LinkedBlockingQueue;

public class Session implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8887985031218619216L;
	private boolean compulsory;
	private String name;
	private String id;
	private LinkedBlockingQueue<Slot> slots;
	private LinkedBlockingQueue<String> studentsSignedUp;
	private String type;
	private String frequency;
	
	
	Session(){
		this.compulsory = false;
		this.name = "Unknown";
		this.id = null;
		this.type = "Unknown";
		this.slots = new LinkedBlockingQueue<Slot>();
		this.studentsSignedUp = new LinkedBlockingQueue<String>();
		this.frequency = "once";
	}

	public Session(String id) {
		this();
		this.id = id;
	}

	Session(String id, String name) {
		this(id);
		this.name = name;
	}

	Session(String id, String name, String type) {
		this(id, name);
		this.type = type;
	}

	Session(String id, String name, String type, boolean compulsory) {
		this(id, name, type);
		this.compulsory = compulsory;
	}

	
	Session(String id, String name, String type, String frequency, boolean compulsory){
		this(id,name, type);
		this.compulsory = compulsory;
		this.frequency = frequency;
	}
	
	Session(String id, String name, String type, boolean compulsory, LinkedBlockingQueue<Slot> slots ){
		this(id,name, type, compulsory);
		this.slots = slots;
	}

	Session(String id, String name, String type, boolean compulsory,
			LinkedBlockingQueue<Slot> slots, LinkedBlockingQueue<String> signUp) {
		this(id, name, type, compulsory, slots);
		this.studentsSignedUp = signUp;
	}

	public void signUpStudent(String studentId) {
		if (!this.studentsSignedUp.contains(studentId))
			this.studentsSignedUp.add(studentId);
	}

	public void unsignStudent(String studentID) {
		this.studentsSignedUp.remove(studentID);
	}

	public boolean isCompulsory() {
		return compulsory;
	}

	public LinkedBlockingQueue<Slot> getSlots() {
		return slots;
	}

	public LinkedBlockingQueue<String> getStudentsSignedUp() {
		return studentsSignedUp;
	}

	public String getType() {
		return type;
	}
	
	public String getID(){
		return this.id;
	}
	
	public boolean hasStudent(String studentid){
		return studentsSignedUp.contains(studentid);
	}

	/* UNIMPORTANT METHODS -------------------------------------- */

	public void setType(String type) {
		this.type = type;
	}

	public void setCompulsory(boolean compulsary) {
		this.compulsory = compulsary;
	}

	public void setSlots(LinkedBlockingQueue<Slot> slots) {
		this.slots = slots;
	}

	public void setStudentsSignedUp(LinkedBlockingQueue<String> studentsSignedUp) {
		this.studentsSignedUp = studentsSignedUp;
	}

	public boolean addSlot(Slot slot){
		try{
		this.slots.add(slot);	return true;
		}
		catch(Exception e){
			return false;
		}
	}
	
	public void setFrequency(String freq){
		this.frequency = freq;
	}
	
	public void removeStudent(String studentid){
		Iterator<Slot> iterator = slots.iterator();
		while (iterator.hasNext()){
			iterator.next().removeStudent(studentid);
			
		}
	}

}
