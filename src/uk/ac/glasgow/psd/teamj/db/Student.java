package uk.ac.glasgow.psd.teamj.db;

import java.io.Serializable;
import java.util.HashSet;

public class Student extends User implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3574729701503165961L;
	
	private HashSet<String> courses; 
	
	public Student(String id, String name, String surname) {
		super(id, name, surname);
		setEmail(id + "@student.gla.ac.uk");
		courses = new HashSet<String>();
	}
	
	public synchronized HashSet<String> getCourses(){return courses;}
	
	public synchronized void addCourse(String course){
		courses.add(course);
		Database.getDB().getCourse(course).addStudent(getName());
	}
	
	@Override
	public String getType() {
		return "student";
	}
	

	
}
