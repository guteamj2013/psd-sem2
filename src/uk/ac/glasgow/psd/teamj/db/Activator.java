package uk.ac.glasgow.psd.teamj.db;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

public class Activator implements BundleActivator {

	private ServiceRegistration<Database> databaseHandler;

	@Override
	public void start(BundleContext context) throws Exception {
		databaseHandler = context.registerService(Database.class,
				Database.getDB(), null);

	}

	@Override
	public void stop(BundleContext context) throws Exception {
		Database.getDB().saveDB();
		databaseHandler.unregister();
	}

}
