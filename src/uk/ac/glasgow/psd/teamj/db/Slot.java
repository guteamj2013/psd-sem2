package uk.ac.glasgow.psd.teamj.db;

import java.io.Serializable;
import java.sql.Time;
import java.util.GregorianCalendar ;
import java.util.concurrent.ArrayBlockingQueue;


public class Slot implements Serializable {

	private static final long serialVersionUID = 3048372005010657513L;
	private String room;
	private String building;
	private String slotID;
	private ArrayBlockingQueue<String> staff;
	private ArrayBlockingQueue<String> students;
	private int capacity;    
	private GregorianCalendar startDateTime;
        private GregorianCalendar endDateTime;
	
	 Slot(String room, String building, String sessionID, ArrayBlockingQueue<String> staff,
			ArrayBlockingQueue<String> students, int capacity, GregorianCalendar startDateTime, GregorianCalendar endDateTime){
		this.room = room;
		this.building = building;
		this.slotID = sessionID;
		this.staff = new ArrayBlockingQueue<String>(capacity);
		this.staff.addAll(staff);
		this.students = new ArrayBlockingQueue<String>(capacity);
		this.students.addAll(staff);
		this.capacity = capacity;
		this.startDateTime = startDateTime;
                this.endDateTime = endDateTime;
	}
	
	 public Slot(String room, String building, String sessionID, int capacity, GregorianCalendar startDateTime, GregorianCalendar endDateTime){
		this.room = room;
		this.building = building;
		this.slotID = sessionID;
		this.staff = new ArrayBlockingQueue<String>(capacity);
		this.students = new ArrayBlockingQueue<String>(capacity);
		this.capacity = capacity;
		this.startDateTime = startDateTime;
                this.endDateTime = endDateTime;
	}
	 
	 public void addStaff(String id){
		 this.staff.add(id);
	 }

	public String getRoom() {
		return room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	public String getBuilding() {
		return building;
	}

	public void setBuilding(String building) {
		this.building = building;
	}

	public String getSessionID() {
		return slotID;
	}

	public void setSessionID(String sessionID) {
		this.slotID = sessionID;
	}

	public ArrayBlockingQueue<String> getStaff() {
		return staff;
	}

	public void setStaff(ArrayBlockingQueue<String> staff) {
		this.staff = staff;
	}

	public ArrayBlockingQueue<String> getStudents() {
		return students;
	}

	public void setStudents(ArrayBlockingQueue<String> students) {
		this.students = students;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public GregorianCalendar getStartDateTime() {
		return startDateTime;
	}

	public void setStartDateTime(GregorianCalendar startDateTime) {
		this.startDateTime = startDateTime;
	}
        
        public GregorianCalendar getEndDateTime() {
		return endDateTime;
	}

	public void setEndDateTime(GregorianCalendar endDateTime) {
		this.endDateTime = endDateTime;
	}
        

	public boolean addStudent(String studentid){
		if(capacity ==students.size()) return false;
		capacity++;
		students.add(studentid);
		return true;
		
	}
	
	public boolean hasStudent(String studentid){
		return students.contains(studentid);
	}
	
	public boolean isFull(){
		return students.size()<capacity;
	}
	
	public void removeStudent(String studentid){
		students.remove(studentid);
	}
	
	public void removeStudents(){
		students = new ArrayBlockingQueue<String>(capacity);
	}
	

}
