package uk.ac.glasgow.psd.teamj.db;

import java.io.Serializable;
import java.util.HashSet;

public class Lecturer extends User  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7656097215872958568L;
	
	private HashSet<String> courses; 
	
	public Lecturer(String id, String name, String surname) {
		super(id, name, surname);
		courses = new HashSet<String>();
		
	}
	
	public synchronized HashSet<String> getCourses(){return courses;}
	
	public synchronized void addCourse(String course){
		courses.add(course);
		Database.getDB().getCourse(course).addLecturer(getName());
	}
	
	@Override
	public String getType() {
		return "lecturer";
	}

	
	
}
