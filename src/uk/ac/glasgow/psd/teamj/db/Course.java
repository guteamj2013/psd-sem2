package uk.ac.glasgow.psd.teamj.db;

import java.io.Serializable;
import java.util.Scanner;
import java.util.concurrent.LinkedBlockingDeque;

public class Course implements Serializable {

	private static final long serialVersionUID = 6178572031043044398L;
	private String cid;
	private String name;
	private String shortName;
	private LinkedBlockingDeque<String> lecturers;
	private String admin;
	private LinkedBlockingDeque<String> students;
	private LinkedBlockingDeque<String> sessions;
	private int capacity = 100; // default course capacity

	
	public LinkedBlockingDeque<String> getCompulsorySessions(){
		LinkedBlockingDeque<String> result = new LinkedBlockingDeque<String>();
		for (String s: sessions){
			if (Database.getDB().getSession(s).isCompulsory()){
				result.add(s);
				
			}
		}
		return result;
	}
	
	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LinkedBlockingDeque<String> getLecturers() {
		return lecturers;
	}

	public void setLecturers(LinkedBlockingDeque<String> lecturers) {
		this.lecturers = lecturers;
	}

	public String getAdmin() {
		return admin;
	}

	public void setAdmin(String admin) {
		this.admin = admin;
	}

	public LinkedBlockingDeque<String> getStudents() {
		return students;
	}

	public void setStudents(LinkedBlockingDeque<String> students) {
		this.students = students;
	}

	public LinkedBlockingDeque<String> getSessions() {
		return sessions;
	}

	public void setSessions(LinkedBlockingDeque<String> sessions) {
		this.sessions = sessions;
	}

	public Course(String cid, String name) {

		this.cid = cid;
		this.name = name;
		this.shortName = generateShortName();
		this.lecturers = new LinkedBlockingDeque<String>();
		this.admin = "";
		this.students = new LinkedBlockingDeque<String>();
		this.sessions = new LinkedBlockingDeque<String>();

	}

	Course(String cid, String name, int capacity) {

		this.cid = cid;
		this.name = name;
		this.shortName = generateShortName();
		this.lecturers = new LinkedBlockingDeque<String>();
		this.admin = "";
		this.students = new LinkedBlockingDeque<String>();
		this.sessions = new LinkedBlockingDeque<String>();

	}

	Course(String cid, String name, String admin) {

		this.cid = cid;
		this.name = name;
		this.shortName = generateShortName();
		this.lecturers = new LinkedBlockingDeque<String>();
		this.admin = admin;
		this.students = new LinkedBlockingDeque<String>();
		this.sessions = new LinkedBlockingDeque<String>();

	}

	Course(String cid, String name, String admin, int capacity) {

		this.cid = cid;
		this.name = name;
		this.shortName = generateShortName();
		this.lecturers = new LinkedBlockingDeque<String>();
		this.admin = admin;
		this.students = new LinkedBlockingDeque<String>();
		this.sessions = new LinkedBlockingDeque<String>();

	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	private String generateShortName() {
		StringBuilder sb = new StringBuilder();
		Scanner s = new Scanner(name);
		while (s.hasNext())
			sb.append(s.next().charAt(0));
		s.close();
		return sb.toString();
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public boolean addLecturer(String id) {
		return lecturers.add(id);
	}

	public boolean removeLecturer(String id) {
		return lecturers.remove(id);
	}

	public boolean addStudent(String id) {
		return students.add(id);
	}

	public boolean removeStudent(String id) {
		return students.remove(id);
	}

	public boolean addSession(String id) {
		return sessions.add(id);
	}

	public boolean removeSession(String id) {
		return sessions.remove(id);
	}

	@Override
	public String toString() {
		return "Course [cid=" + cid + ", name=" + name + ", shortName="
				+ shortName + "]";
	}

}
