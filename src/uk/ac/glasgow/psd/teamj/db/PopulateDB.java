package uk.ac.glasgow.psd.teamj.db;

public class PopulateDB {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Database db = Database.getNewDB();
		db.addCourse("Java Programming 2", "COMPSCI 2001");
		Course course = db.getCourse("COMPSCI 2001");
		course.addSession("JP2Lab1");
		db.addSession("JP2Lab1");
		db.addTutor("Test", "Tutor", "3333333");
		Tutor tutor = db.getTutor("3333333");
		db.addAdmin("Test", "Admin", "0000000");
		Admin admin = db.getAdmin("0000000");
		db.addStudent("Test", "Student", "1111111");
		db.addLecturer("Test", "Lecturer", "2222222");
		db.saveDB();
		
		System.out.println(course);
		
		db = Database.getDB();
	}

}
