package uk.ac.glasgow.psd.teamj.db;

import java.io.Serializable;
import java.util.HashSet;

public abstract class User  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3329862806132334712L;
	
	private String id;
	private String name;
	private String surname;
	private String email;
	
	public User(String id, String name, String surname) {
		this.id = id;
		this.name = name;
		this.surname = surname;
	}
	
	public void setName(String name){this.name = name;}
	public void setSurname(String surname){this.surname = id;}
	public void setEmail(String email){this.email = id;}
	public void setID(String id){this.id = id;}
	
	public String getID(){return id;}
	public String getName(){return name;}
	public String getSurname(){return surname;}
	public String getEmail(){return email;}
	
	public abstract String getType();
	
	/**
	 * check user is attending all compulsory sessions for all courses they are
	 * signed up for
	 */
	public synchronized boolean checkCompulsary() {
		if (!this.getType().equals("student"))
			return false; // if it's not a student, return false.
		Student student = (Student) this;
		HashSet<String> courses = student.getCourses();
		for (String s:courses){
			for (String session : Database.getDB().getCourse(s).getCompulsorySessions()) {
				if (! Database.getDB().getSession(session)
						.getStudentsSignedUp()
						.contains(student))
					return false;	
			}
		}
		
		return true; // the student is signed up in all his compulsory courses.
	}
	
}
