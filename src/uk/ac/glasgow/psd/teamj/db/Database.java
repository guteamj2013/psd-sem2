package uk.ac.glasgow.psd.teamj.db;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.GregorianCalendar;
import java.util.Date;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class Database implements Serializable {

	private static final long serialVersionUID = 7331107298334179511L;
	private static Database database = null;

	@SuppressWarnings("rawtypes")
	private ConcurrentHashMap<String, ConcurrentHashMap> db = null;

	@SuppressWarnings("rawtypes")
	private Database() {
		db = new ConcurrentHashMap<String, ConcurrentHashMap>(7, (float) 1.0);
		ConcurrentHashMap<String, Admin> admins = new ConcurrentHashMap<String, Admin>();
		db.put("admins", admins);
		ConcurrentHashMap<String, Student> students = new ConcurrentHashMap<String, Student>();
		db.put("students", students);
		ConcurrentHashMap<String, Tutor> tutors = new ConcurrentHashMap<String, Tutor>();
		db.put("tutors", tutors);
		ConcurrentHashMap<String, Lecturer> lecturers = new ConcurrentHashMap<String, Lecturer>();
		db.put("lecturers", lecturers);
		ConcurrentHashMap<String, Course> courses = new ConcurrentHashMap<String, Course>();
		db.put("courses", courses);
		ConcurrentHashMap<String, Session> sessions = new ConcurrentHashMap<String, Session>();
		db.put("sessions", sessions);
		ConcurrentHashMap<String, Slot> slots = new ConcurrentHashMap<String, Slot>();
		db.put("slots", slots);
	}

	public static Database getDB() {
		if (database == null) {
			loadDB();
		}
		return database;
	}

	public static Database getNewDB() {
		if (database == null) {
			database = new Database();
		}
		return database;
	}

	public void saveDB() {
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("db.ser"));
			oos.writeObject(database);
			oos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 

	}

	private static void loadDB() {
		try  {
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream("db.ser"));
			database = (Database) ois.readObject();
			ois.close();
		} catch (FileNotFoundException e) {
			getNewDB();
			// e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public Student getStudent(String id) {
		return (Student) db.get("students").get(id);
	}

	@SuppressWarnings("unchecked")
	public void addStudent(String name, String surname, String id) {
		db.get("students").put(id, new Student(id, name, surname));
	}

	public Tutor getTutor(String id) {
		return (Tutor) db.get("tutors").get(id);
	}

	@SuppressWarnings("unchecked")
	public void addTutor(String name, String surname, String id) {
		db.get("tutors").put(id, new Tutor(id, name, surname));
	}

	public Lecturer getLecturer(String id) {
		return (Lecturer) db.get("lecturers").get(id);
	}

	@SuppressWarnings("unchecked")
	public void addLecturer(String name, String surname, String id) {
		db.get("lecturers").put(id, new Lecturer(id, name, surname));
	}

	public Admin getAdmin(String id) {
		return (Admin) db.get("admins").get(id);
	}

	@SuppressWarnings("unchecked")
	public void addAdmin(String name, String surname, String id) {
		db.get("admins").put(id, new Admin(id, name, surname));
	}

	public Course getCourse(String id) {
		return (Course) db.get("courses").get(id);
	}

	@SuppressWarnings("unchecked")
	public Object addCourse(String courseName, String courseID) {
		return db.get("courses").put(courseID, new Course(courseID, courseName));
	}

	@SuppressWarnings("unchecked")
	public void addCourse(String courseName, String courseID, String admin) {
		db.get("courses")
				.put(courseID, new Course(courseID, courseName, admin));
	}

	@SuppressWarnings("unchecked")
	public void addCourse(String courseName, String courseID, int capacity) {
		db.get("courses").put(courseID,
				new Course(courseID, courseName, capacity));
	}

	@SuppressWarnings("unchecked")
	public void addCourse(String courseName, String courseID, String admin,
			int capacity) {
		db.get("courses").put(courseID,
				new Course(courseID, courseName, admin, capacity));
	}

	public Session getSession(String id) {
		return (Session) db.get("sessions").get(id);
	}

	@SuppressWarnings("unchecked")
	public void addSession(String id) {
		db.get("sessions").put(id, new Session(id));
	}

	@SuppressWarnings("unchecked")
	public void addSession(String id, String name) {
		db.get("sessions").put(id, new Session(id, name));
	}

	@SuppressWarnings("unchecked")
	public void addSession(String id, String name, String type) {
		db.get("sessions").put(id, new Session(id, name, type));
	}

	@SuppressWarnings("unchecked")
	public void addSession(String id, String name, String type,
			boolean compulsory) {
		db.get("sessions").put(id, new Session(id, name, type, compulsory));
	}

	public Slot getSlot(String id) {
		return (Slot) db.get("slots").get(id);
	}

	@SuppressWarnings("unchecked")
	public void addSlot(String room, String building, String slotID,
			int capacity,GregorianCalendar startDateTime, GregorianCalendar endDateTime) {
		db.get("slots").put(slotID,
				new Slot(room, building, slotID, capacity, startDateTime, endDateTime));
	}

	@SuppressWarnings("unchecked")
	public Set<String> getUsers() {
		Set<String> result = db.get("students").keySet();
		result.addAll(db.get("tutors").keySet());
		result.addAll(db.get("lecturers").keySet());
		result.addAll(db.get("admins").keySet());
		return result;
	}
	
	public int getUserCount(){
		return db.get("students").size() 
				+ db.get("tutors").size() 
				+ db.get("lecturers").size()
				+ db.get("admins").size();
	}

	@SuppressWarnings("unchecked")
	public Set<String> getStudents() {
		return db.get("students").keySet();
	}

	@SuppressWarnings("unchecked")
	public Set<String> getTutors() {
		return db.get("tutors").keySet();
	}

	@SuppressWarnings("unchecked")
	public Set<String> getLecturers() {
		return db.get("lecturers").keySet();
	}

	@SuppressWarnings("unchecked")
	public Set<String> getAdmins() {
		return db.get("admins").keySet();
	}

	@SuppressWarnings("unchecked")
	public Set<String> getCourses() {
		return db.get("courses").keySet();
	}

	@SuppressWarnings("unchecked")
	public Set<String> getSessions() {
		return db.get("sessions").keySet();
	}

	@SuppressWarnings("unchecked")
	public Set<String> getSlots() {
		return db.get("slots").keySet();
	}
	

}
