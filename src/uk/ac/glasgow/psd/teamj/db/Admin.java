package uk.ac.glasgow.psd.teamj.db;

import java.io.Serializable;

public class Admin extends User  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3720939337556993371L;

	public Admin(String id, String name, String surname) {
		super(id, name, surname);
		
	}

	@Override
	public String getType() {
		return "administrator";
	}
	
}
