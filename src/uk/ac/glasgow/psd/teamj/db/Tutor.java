package uk.ac.glasgow.psd.teamj.db;

import java.io.Serializable;
import java.util.HashSet;

public class Tutor extends User  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1119514412711216528L;
	
	private HashSet<String> slots;

	public Tutor(String id, String name, String surname) {
		super(id, name, surname);
		slots = new HashSet<String>();
		
	}
	
	public synchronized HashSet<String> getSlots(){return slots;}
	
	public synchronized void addSlot(String slot){
		slots.add(slot);
		Database.getDB().getSlot(slot).addStaff(getName());
	}
	@Override
	public String getType() {
		return "tutor";
	}
	
	
}
