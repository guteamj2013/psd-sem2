Narrative: Lecturer Imports Course
As a lecturer,
I want to import a MyCampus course,
So that teaching sessions can be identified.

Scenario: 
Given course not in the system by a lecturer/administrator
When trying to import the course
Then the course exists in the database

Scenario: 
Given course not in the system by a lecturer/administrator
When trying to import the course
Then the course is added to the database

Scenario: 
Given course already in the system by a lecturer/administrator
When trying to import the course
Then adding the duplicate entry fails

Scenario: 
Given an invalid course by a lecturer/administrator
When trying to import a course
Then the system will not add the session to the DB
	
Scenario: security
Given any course by a student or tutor
When trying to import a course
Then the system will not add the course as the user is not privileged.