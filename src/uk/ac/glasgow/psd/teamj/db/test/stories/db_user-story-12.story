Narrative: Student checking compulsory sessions
As a student,
I want to check that I have signed up for all compulsory sessions,
So that I don't fail the course.

Scenario:
Given a student type
When the student has not yet enrolled for any courses
Then the method should return true

Scenario:
Given a student
When the student does not have any compulsory sessions, but has optional sessions
Then the method should return true

Scenario:
Given a student
When the student has compulsory sessions
And is not signed up for one compulsory session
Then the method should return false

Scenario:
Given a student
When the student has compulsory sessions
And is signed up for all compulsory sessions
Then the method should return true
	
Scenario:
Given an admin
Then the method should return true, (as they dont sign up for sessions)