Narrative:
The system shall support at least 100 courses.

Scenario:
Given a new course
When there are less than 100 existing courses
Then the course will be added to the database  

Scenario:
Given a new course
When there are 100 or more existing courses already
Then the course will be added to the database or the action will fail                                                                                                   
