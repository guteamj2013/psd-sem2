Narrative: 
As a lecturer, I want to specify that a session is a one off, or recurs weekly or fortnightly, So that
I don't have to create a large number of sessions.

Scenario: 
Given a frequency by a lecturer/admin
When creating a new session
Then the session will be created with the given frequency 


Scenario:
Given a frequency by a lecturer/admin
When editing an existing session
Then the user will have the option to change its frequency 

Scenario:
Given a student type
Then calling addSession will throw a PermissionDeniedException as they are not privileged
And calling editExistingSession will throw a PermissionDeniedException as they are not privileged

Scenario:
Given a tutor type
Then calling addSession will throw a PermissionDeniedException as they are not privileged
And calling editExistingSession will throw a PermissionDeniedException as they are not privileged