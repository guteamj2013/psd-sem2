package uk.ac.glasgow.psd.teamj.db.test.steps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.concurrent.LinkedBlockingQueue;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import uk.ac.glasgow.psd.teamj.db.Admin;
import uk.ac.glasgow.psd.teamj.db.Course;
import uk.ac.glasgow.psd.teamj.db.Database;
import uk.ac.glasgow.psd.teamj.db.Lecturer;
import uk.ac.glasgow.psd.teamj.db.Slot;
import uk.ac.glasgow.psd.teamj.db.Student;
import uk.ac.glasgow.psd.teamj.db.Tutor;
import uk.ac.glasgow.psd.teamj.db.User;
import uk.ac.glasgow.psd.teamj.myCampus.MyCampusConnection;
import uk.ac.glasgow.psd.teamj.user.UserFactory;
import uk.ac.glasgow.psd.teamj.session.PermissionDeniedException;
import uk.ac.glasgow.psd.teamj.session.Session;
import uk.ac.glasgow.psd.teamj.session.SessionInterface;

public class DatabaseSteps {
	private Database db = Database.getDB();
	private MyCampusConnection mcComponent = new MyCampusConnection(db);
	private SessionInterface sessionComponent = new Session(db);
	private String sessionID;
	private boolean compulsary;
	private String frequency;
	private String courseID;
        private String course2ID;
        private Slot slotCheck;
	String room;
	String building;
	String slotID;
	private User user;
	boolean success;

	private Course course;
	private LinkedBlockingQueue<Slot> slots;
	private int ten;
	private String newSesName;
		
	
		// performance 0 tests
		@Given("a new course")
		public void newCourse() {
			courseID = "COMPSCI";
		}

		@When("there are less than $n existing courses")
		public void addhundredCourses(int n) {
		
			System.out.println("add 100");
			success = true;
			int beforeAdding, afterAdding;
			for (int i = 0; db.getCourses().size() <= n && i < n; i++) {
				beforeAdding = db.getCourses().size();
				db.addCourse("course " + i, courseID + i);
				afterAdding = db.getCourses().size();
				success = (afterAdding - beforeAdding) == 1;
			
			}
		}

		@Then("the course will be added to the database")
		public void hasHundredCourses() {
			System.out.println("has 100");
			assertThat(this.success, equalTo(true));
		}

		@When("there are $n or more existing courses already")
		public void addMoreCourses(int n) {
			System.out.println("add one more course");
			for (int i = n; db.getCourses().size() <= n && i <= 2*n; i++) {
				db.addCourse("course " + i, courseID + i);
			}
			success = (db.getCourses().size() <= n+1);
		}

		@Then("the course will be added to the database or the action will fail")
		public void moreCoursesAdded() {
			assertThat(this.success, equalTo(true));
		}
		
		// Performance 1 Steps

		@Given("an existing course sc2014")
		public void aNewCourse() {
			db.addCourse("sample course", "sc2014");
			course = db.getCourse("sc2014");
		}

		@Then("it can simultaneously have $number session types session0-sessionX")
		public void isXTypespossible(int number) {
			int count = 0;
			for (int i = 0; i < number; i++) {
				db.addSession("session" + i, "Test session " + i, "" + i);
				course.addSession("" + i);
				if (course.getSessions().contains(i + "")) {
					count++;
				}
			}
			assertThat(count, equalTo(number));
		}
	
		
		// Performance 3 Steps
		
		private String roomNum;
		
		@Given("a new slot for a session $sessionName")
		public void newSessionSlot(String sessionName) {
			User u = db.getAdmin("0000000");
			slotID = "group ";
			roomNum = "302";
			building = "Boyd Orr Building";
			uk.ac.glasgow.psd.teamj.db.Session newSes = new uk.ac.glasgow.psd.teamj.db.Session(sessionName);
			db.addSession(newSes.getID());
			newSesName = sessionName;
		}

		@When("session has less than $number slots assigned to it already")
		public void lessthanNslots(int number) {
			assertThat(true, equalTo(db.getSession(newSesName).getSlots().size()<number));
		}

		@Then("the slot should be assigned to the session")
		public void slotAssigned() {
			uk.ac.glasgow.psd.teamj.db.Session dbsession = db.getSession(this.newSesName);
			boolean success = dbsession.addSlot(new Slot("test", "test", "test", 100, new GregorianCalendar(),new GregorianCalendar()));	
			assertThat(success, equalTo(true));
			
		}

		@When("session have $number or more sessions assigned to it")
		public void nOrMoreSessions(int number) {
			for (int i = 0; i<number; i++){
				db.getSession(newSesName).addSlot(new Slot(i+"", i+"", i+"", 100, new GregorianCalendar(),new GregorianCalendar()));
			}
			assertThat(true, equalTo(db.getSession(newSesName).getSlots().size() >= number));
		}

		@Then("the slot may be assigned to the session or assignment may fail")
		public void mayAssignSession() {
			uk.ac.glasgow.psd.teamj.db.Session dbsession = db.getSession(this.newSesName);
			boolean success = dbsession.addSlot(new Slot("test1", "test1", "test1", 100, new GregorianCalendar(),new GregorianCalendar()));	
			assertThat(success, isOneOf(true, false));
		}
		// User Story 1 Steps

		@Given("course not in the system by a lecturer/administrator")
		public void aValidCourse() {
			user = db.getAdmin("0000000");
			courseID = "TEST";

		}

		@When("trying to import the course")
		public void importCourse() throws PermissionDeniedException {
			success = mcComponent.importCourse(user, courseID);

		}

		@Then("the course is added to the database")
		public void isCourseAdded() {
			assertThat(this.success, equalTo(true));

		}

		@Given("course already in the system by a lecturer/administrator")
		public void validCourseAlreadyExisting() throws PermissionDeniedException {
			user = db.getAdmin("0000000"); // Admin ID
			courseID = "COMPSCI 2001";
			//db.addCourse("Java Programming 2", courseID);

			// now that course is added it exists in the DB already.
		}

		@Then("adding the duplicate entry fails")
		public void CourseAddingFail(){
			assertThat(success, equalTo(false));

		}

		@Given("an invalid course by a lecturer/administrator")
		public void anInvalidCourse() {
			user = db.getAdmin("1234567"); // random ID
			courseID = null;
		}

		@Then("the system will not add the session to the DB")
		public void CourseNotAdded() throws PermissionDeniedException {
			success = mcComponent.importCourse(user, courseID);
			assertThat(success, equalTo(false));
		}

		@Given("any course by a student or tutor")
		public void courseInvalidUser() {
			user = db.getStudent("111111"); // student ID

		}

		@Then("the system will not add the session as the user is not privileged")
		public void courseNotAddedExceptionThrown() {
			try {
				mcComponent.importCourse(user, courseID);
			} catch (Exception e) {
				// If it comes here exception is throw and course is not added
				assertThat(true, equalTo(true));
			}
		}
		// User Story 4 Steps **************************************************

		@Given("a frequency by a lecturer/admin")
		public void aFrequency() {
			frequency = "once";
			user = db.getAdmin("0000000"); // Admin ID

		}

		@When("creating a new session")
		public void createSessionDetails() {
			courseID = "COMPSCI 2001";
			sessionID = "JPlab1";
			compulsary = false;
		}

		@Then("the session will be created with the given frequency")
		public void isAddedwithFrequency() throws PermissionDeniedException {
			success = sessionComponent.addSession(user, courseID, sessionID,
					frequency, compulsary);
			assertThat(success, equalTo(true));
		}

		@When("editing an existing session")
		public void editSession() {
			sessionID = "JPlab1";
			db.addSession(sessionID);

		}

		@Then("the user will have the option to change its frequency")
		public void changeFrequency() {
			success = true;
			try {
				sessionComponent.editExistingSession(user, sessionID, building, false);
			} catch (PermissionDeniedException e) {
				success = false;
			}
			assertThat(success, equalTo(true));
		}

		@Given("a $userType type")
		public void afrequencyByInvalidUser(String userType) {
			if (userType.equalsIgnoreCase("student")){
				user = new Student("test", "test", "test");
			}
			else{
				user = new Tutor("test", "test", "test");
			}
		}
		
		@Then("calling addSession will throw a PermissionDeniedException as they are not privileged")
		public void callMethods(){
			success = false;
			try {
				sessionComponent.addSession(user, "test", "test", "test", true);
			} catch (PermissionDeniedException e) {
				success = true; 
			}
			assertThat(success, equalTo(true));
		}
		
		@Then("calling editExistingSession will throw a PermissionDeniedException as they are not privileged")
		public void editExistingSession(){
			success = false;
			try {
				sessionComponent.editExistingSession(user, "test", "test", false);
			} catch (PermissionDeniedException e) {
				success = true; 
			}
			assertThat(success, equalTo(true));
		}
		
		// User Story 8 Steps -----------------------------------------------------------

		@Given("a room number $num")
		public void newRoom(String num) {
			user = db.getAdmin("0000000");
			sessionID = "JP2Lab1";
			room = num;
			building = "Boyd Orr Building";
			slotID = "group A";

		}

		@When("the slot does not have a room assigned to it")
		public void addSlotNoRoom() {
			db.addSlot(null, null, slotID, 150, new GregorianCalendar(),
					new GregorianCalendar());

		}

		@Then("the room number should be assigned to the timetable slot")
		public void assignRoom() throws PermissionDeniedException {
			boolean success = sessionComponent.assignRoom(user, courseID,
					sessionID, slotID, building, room);
			assertThat(success, equalTo(true));
		}

		@When("the slot already has a room assigned to it")
		public void addSlotAssignRoom() {
			db.addSlot(room, building, slotID, 150, new GregorianCalendar(),
					new GregorianCalendar());
		}

		@Then("the new room number should replace the previous number assigned to the slot.")
		public void replaceRoom() throws PermissionDeniedException {
			boolean success = sessionComponent.assignRoom(user, courseID,
					sessionID, slotID, building, room);
			assertThat(success, equalTo(true));
		}
		
		// User Story 14 Steps

		@Given("a valid UoG course $course and session ID $session")
		public void validDetails(String course, String session) {
			sessionID = session;
			courseID = course;
		}

		@When("trying to check the slot details of a session")
		public void checkingDetails() {

		}

		@Then("the session details are retrieved from the database")
		public void detailsRetrieved() {
			ArrayList<String> details = sessionComponent.slotDetails(courseID,
					sessionID);
			assertThat(details, notNullValue());
		}

		@Given("an invalid session ID $session for valid course $course")
		public void invalidID(String session, String course) {
			sessionID = session;
			courseID = course;
		}

		@Then("the details will not be retrieved from the database")
		public void detailsNotRetrieved() {
			ArrayList<String> details = sessionComponent.slotDetails(courseID,
					sessionID);
			assertThat(details, equalTo(null));
		}

		
		
		// User Story 11 Steps

		@Given("a valid session slot by a student")
		public void aValidSlot() {
		courseID = "COMPSCI 2001";
		slotID = "group A";
		sessionID = "JP2Lab1";
		user = db.getStudent("1111111");
		}

		@When("the student has not enrolled for the slots course")
		public void bookSlotForInvalidCourse() {
		db.getCourse(courseID).removeStudent(user.getID());
		success = (!db.getSlot(slotID).addStudent(user.getID()) && !db.getSlot(
		slotID).hasStudent(user.getID()));

		}

		@Then("the student will not be added and method returns false")
		public void isTestSuccessful() {
		assertThat(this.success, equalTo(true));
		}


		@When("the student has not enrolled for that session")
		public void UnenrollFromSession() {
		db.getSession(sessionID).removeStudent(user.getID());
		}

		@When("the student is enrolled for that course")
		public void enrollForCourse() {
		db.getCourse(courseID).addStudent(user.getID());
		}

		@When("the slot is already at capacity")
		public void slotNotFull() {
		db.getSlot(slotID).setCapacity(db.getSlot(slotID).getCapacity() + 1);
		}

		@Then("the student is not added and method returns false")
		public void isStudentAdded() {
		success = (db.getSlot(slotID).addStudent(user.getID()) && db.getSlot(
		slotID).hasStudent(user.getID()));
		assertThat(this.success, equalTo(true));
		}
		/*

		@When("the student has not enrolled for that session")
		public void UnenrollFromSession2() {
		db.getSession(sessionID).removeStudent(user.getID());
		}

		@When("the student is enrolled for that course")
		public void enrollForCourse2() {
		db.getCourse(courseID).addStudent(user.getID());
		}

		@When("the slot is already at capacity")
		public void slotFull() {
		db.getSlot(slotID).setCapacity(0);
		}

		@Then("the student is not added and method returns false")
		public void studentNotAdded() {
		success = (!db.getSlot(slotID).addStudent(user.getID()) && !db.getSlot(
		slotID).hasStudent(user.getID()));
		assertThat(this.success, equalTo(true));
		}

		@When("the student has already enrolled for that session")
		public void studentEnrolledForSession() {
		db.getSession(sessionID).signUpStudent(user.getID());
		success = (!db.getSlot(slotID).addStudent(user.getID()) && !db.getSlot(
		slotID).hasStudent(user.getID()));
		}

		@Then("the student is not enrolled for the new slot and method returns false")
		public void studentNotEnrolled() {
		assertThat(this.success, equalTo(true));
		}

		@When("the student has already enrolled for that session slot")
		public void studentEnrolledForSlot() {
		db.getSlot(slotID).addStudent(user.getID());
		success = (!db.getSlot(slotID).addStudent(user.getID()) && db.getSlot(
		slotID).hasStudent(user.getID()));
		}

		@Then("the slot stays booked by the student and method return false")
		public void studentNotEnrolled2() {
		assertThat(this.success, equalTo(true));
		}

		@Given("an invalid course slot by a student")
		public void anInvalidSlot() {
		slotID = "nonexistent slot";
		user = db.getStudent("1111111");
		}

		@Then("the student is not enrolled and method returns false")
		public void nothingChanges() {
		success = ((db.getSlot("group X") == null));
		assertThat(this.success, equalTo(true));
		}

		@Given("a course slot by a user")
		public void slotByUser() {
		courseID = "COMPSCI 2001";
		slotID = "group A";
		user = db.getAdmin("0000000");
		}

		@When("the user is not a student")
		public void nonStudent() {
		success = (!db.getSlot(slotID).addStudent(user.getID())
		&& !user.getType().equals("student") && !db.getSlot(slotID)
		.hasStudent(user.getID()));
		}

		@Then("the user is not enrolled and method returns false")
		public void userNotAddedtoSlot() {
		assertThat(this.success, equalTo(true));
		}

		*/
}
