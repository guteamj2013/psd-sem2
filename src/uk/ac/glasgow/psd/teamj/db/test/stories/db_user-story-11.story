Narrative: Student booking a slot

As a student,
I want to book a timetable slot for each session of my course,
So that I can take the course.

Scenario: 
Given a valid session slot by a student
When the student has not enrolled for the slots course
Then the student will not be added and method returns false
	
Scenario:
Given a valid course slot by a student
When the slot is not booked by the student
Then the student is added and method returns true
	
Scenario:
Given a valid session slot by a student
When the student has not enrolled for that session
And the student is enrolled for that course
And the slot is already at capacity
Then the student is not added and method returns false

Scenario:
Given a valid session slot by a student
When the student has already enrolled for that session
Then the student is not enrolled for the new slot and method returns false

Scenario:
Given a valid session slot by a student
When the student has already enrolled for that session slot
Then the slot stays booked by the student and method return false

Scenario:
Given an invalid course slot by a student
Then the student is not enrolled and method returns false

Scenario:
Given a course slot by a user
When the user is not a student
Then the user is not enrolled and method returns false
