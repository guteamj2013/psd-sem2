Narrative: Lecturer adding a session to a course
As a lecturer,
I want to add a session to a course,
So that timetable slots can be identified.
	
Scenario:
Given a valid session not in the database by a lecturer/administrator
When trying to add a session to a course
Then the session is added to the database
And the method will return true
	
Scenario:
Given a valid session already in the database by a lecturer/administrator
When trying to add a session to a course
Then the session is not added
And the method will return false
		
Scenario: 
Given an invalid session by a lecturer/administrator
When trying to add a session to a course
Then the system will not add the session to the DB.
And the method will return false
	
Scenario:
Given a session by a student or tutor
When trying to add a session to a course
Then the system will not add the session as the user is not privileged.
And the method will return false