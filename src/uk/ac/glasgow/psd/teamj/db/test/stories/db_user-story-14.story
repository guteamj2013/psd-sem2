Narrative: Lecturer checking the details of a slot
As a lecturer,
I want to see the details (time, location, students, tutor) or every timetable slot in a session,
So that I know when sessions happen.
	
Scenario:
Given a valid UoG course COMPSCI 2001 and session ID JP2Lab1
When trying to check the slot details of a session
Then the session details are retrieved from the database
	
Scenario:
Given an invalid session ID DFGFSFGDFCXCG for valid course COMPSCI 2001
When trying to check the slot details of a session
Then the details will not be retrieved from the database