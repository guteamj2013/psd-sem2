Narrative: Lecturer Imports Course
As a administrator,
I want to assign a room to a timetable slot,
So that room bookings can be recorded.

Scenario: 
Given a room number 504d
When the slot does not have a room assigned to it
Then the room number should be assigned to the timetable slot

Scenario: 
Given a room number 507d
When the slot already has a room assigned to it
Then the new room number should replace the previous number assigned to the slot.