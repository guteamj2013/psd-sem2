package uk.ac.glasgow.psd.teamj.user;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.osgi.framework.ServiceReference;



import uk.ac.glasgow.psd.teamj.db.Database;


public class Activator implements BundleActivator {

	private ServiceRegistration<UserFactoryInterface> 
		userFactoryRegistration;
	
	
	@Override
	public void start(BundleContext context)
		throws Exception {
		
		ServiceReference<Database> serviceReference =
				context.getServiceReference(Database.class);
		
		Database db = context.getService(serviceReference);
		
		UserFactoryInterface userFactoryHandler = new UserFactory(db);
		
		
		userFactoryRegistration = 
			context.registerService(
				UserFactoryInterface.class, userFactoryHandler, null);	
	}	
	
	@Override
	public void stop(BundleContext context)
		throws Exception {
		
		userFactoryRegistration.unregister();
	}
}