package uk.ac.glasgow.psd.teamj.user;

import uk.ac.glasgow.psd.teamj.db.User;
import uk.ac.glasgow.psd.teamj.myCampus.MyCampus;

public interface UserFactoryInterface {
	
	public User login(String id, String pwd);
	
	public boolean register(User user,String pass);
	

}
