package uk.ac.glasgow.psd.teamj.user;

import uk.ac.glasgow.psd.teamj.db.Database;
import uk.ac.glasgow.psd.teamj.db.User;
import uk.ac.glasgow.psd.teamj.myCampus.MyCampus;
import uk.ac.glasgow.psd.teamj.myCampus.MyCampusConnection;

public class UserFactory implements UserFactoryInterface{
	private Database db;
	MyCampusConnection connection;
	public UserFactory(Database db){
		this.db = db;
		connection = new MyCampusConnection(db);
	}
	
	public User login(String id, String pwd){
		String clearance = connection.Authenticate(id, pwd);
		//System.out.println(clearance);
		if (clearance==null) return null;
		else if (clearance.equals("administrator")) return db.getAdmin(id);
		else if (clearance.equals("student")) return db.getStudent(id);
		else if (clearance.equals("lecturer")) return db.getLecturer(id);
		else if (clearance.equals("tutor")) return db.getTutor(id);
        
        else return null;
	}
	
	public boolean register(User user,String pass){
		try{
			MyCampus.addNewUser(user, pass);
			return true;
		}catch(Exception e ){
			e.printStackTrace();
			return false;
		}
	}
}
