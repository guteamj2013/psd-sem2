Narrative:
The system shall support at least 1000 different users.

Scenario:
Given new user to the system
When the system has less than 1000 users
Then the system has 1 more user
