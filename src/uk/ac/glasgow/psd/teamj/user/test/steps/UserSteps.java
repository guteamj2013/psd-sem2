package uk.ac.glasgow.psd.teamj.user.test.steps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import java.util.Random;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import uk.ac.glasgow.psd.teamj.db.Admin;
import uk.ac.glasgow.psd.teamj.db.Database;
import uk.ac.glasgow.psd.teamj.db.Lecturer;
import uk.ac.glasgow.psd.teamj.db.Student;
import uk.ac.glasgow.psd.teamj.db.Tutor;
import uk.ac.glasgow.psd.teamj.db.User;
import uk.ac.glasgow.psd.teamj.myCampus.MyCampus;
import uk.ac.glasgow.psd.teamj.myCampus.MyCampusConnection;
import uk.ac.glasgow.psd.teamj.user.UserFactory;
import uk.ac.glasgow.psd.teamj.session.PermissionDeniedException;
import uk.ac.glasgow.psd.teamj.session.Session;

public class UserSteps {

	private UserFactory factory = new UserFactory(Database.getDB());
	private MyCampusConnection mycampus = new MyCampusConnection(Database.getDB());
	private String logIn;
	User userInstance;
	private String password;
	private boolean notDone = true;
	private int loopCount =0;

	private void createUser(String id, String name, String surname) {
		User temp = new Admin(id, name, surname);
		Database.getDB().addAdmin(name, surname, id);
		factory.register(temp,"pass");
	}
	

	// **************************	PERFORMACE 2 SCENARIOS ************************************//
	
	// register one admin
	@Given ("new user to the system")
	public void register_user(){
		User temp = new Admin("new", "new", "new");
		Database.getDB().addAdmin("new", "new", "new");
		factory.register(temp,"pass");
	}
	
	// check that the database has less than the number of users needed
	@When("the system has less than $number users")
	public void createUsers(int number) {
		loopCount = number;
		assertThat(Database.getDB().getUserCount() <number, equalTo(true) );
	}
	
	// add 1 more user in each loop and check if it was added every single time. stop at given number
	@Then("the system has 1 more user")
	public void numberUsers() {
		int beforeAdding;
		int afterAdding;
		boolean result = true;
		for (int i = 0; i < loopCount; i++) {
			beforeAdding = Database.getDB().getUserCount();
			createUser(i + "", i + "", i + "");
			afterAdding = Database.getDB().getUserCount();
			result = (afterAdding - beforeAdding) == 1;
		}
		assertThat(true, equalTo(result));
	}
		
	
	
	//******************** PERFORMACE 4 SCAARIOS ***********************//
	
	// creates a number of users in the database and creates a thread for each one and logs them in
	// the thread just acts as a busy logged in user of the system.
	@Given("$number users logged in")
	public void logInUsers(int number) {
		this.setNotDone(true);
		for (int i = 0; i < number; i++) {
			createUser(i+"a",i+"a",i+"a");
			Worker w = new Worker(factory.login(i + "a", "pass"));
			Thread t = new Thread(w);
			t.start();
		}
		
	}
	
	private class Worker implements Runnable {
		User user;
		Worker(User u) {
			this.user = u;
		}
		@Override
		public void run() {
			// loops until the test is over and threads need to be killed
			while (getNotDone()) {			
				try {
					// the importing will fail as the users are students, but it will keep the system a bit busy
					mycampus.importCourse(user, "test");
				} catch (PermissionDeniedException e) {}
			}
		}
	}
	
	// thread safe methods for the loops to check if the test is completed
	
	public synchronized boolean getNotDone() {
		return notDone;
	}

	public synchronized void setNotDone(boolean setTo) {
		notDone = setTo;
	}
	
	// log in one more user
	@When ("a user logs in")
	public void logIn_a_User(){
		register_user(); // register a user with credentials new new new
		Worker w = new Worker(factory.login("new","pass"));
		Thread t = new Thread(w);
		t.start();
	}
	
	// check that user thread count is equal to the number of expected users
	@Then ("there are $number users logged in")
	public void check_loggedIn_users(int number){
		assertThat(Thread.activeCount()-3,equalTo(number));	// take away 3 threads which are not "users" main and system threads, etc
		this.setNotDone(false);	// signal that test is over
		while (Thread.activeCount() > 3){this.setNotDone(false);}	// wait for threads to die before starting another test
	}
	

	@Given("a $user")
	public void createNewUser(String user) {
		
		if (user.equalsIgnoreCase("student")) {
			userInstance = new Student("1", "student", "student");
			Database.getDB().addStudent("test", "test", "1");
		} else if (user.equalsIgnoreCase("lecturer")) {
			userInstance = new Lecturer("1", "lecturer", "lecturer");
			Database.getDB().addLecturer("test", "test", "1");
		} else if (user.equalsIgnoreCase("administrator")) {
			userInstance = new Admin("1", "admin", "admin");
			Database.getDB().addAdmin("test", "test", "1");
		} else if (user.equalsIgnoreCase("tutor")) {
			userInstance = new Tutor("1", "tutor", "tutor");
			Database.getDB().addTutor("test", "test", "1");
		}
		else System.out.println(user);
		this.createUser("1", "new", "new");
		factory.register(userInstance, "pass");

	}

	@Given("random log in combination")
	public void randomLogIn() {
		Random randomLogins = new Random();
		logIn = ((Integer) randomLogins.nextInt(50)).toString();
		password = ((Integer) randomLogins.nextInt(50)).toString();

	}

	@When("log in is $isCorrect")
	public void isLogInCorrect(String isCorrect) {
		
		if (isCorrect.equalsIgnoreCase("correct"))
			{
			logIn = userInstance.getID();
			password = "pass";
		}
		else {
			logIn = "random";
			password = "nonsense";
		}
		
	}

	@Then("$user instance will be returned")
	public void returnedInstance(String user) {
		
		User userAuthenticated = factory.login(logIn, password);
		assertThat(user, equalTo(userAuthenticated.getType()));
	}

	@Then("null will be returned")
	public void nullReturned() {

		User userAuthenticated = factory.login(logIn, password);
		if (userAuthenticated == null)
			assertThat(true, equalTo(true));
		else
			assertThat(false, equalTo(true));
	}

}
