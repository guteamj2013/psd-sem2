Narrative:
The system shall support at least 100 different concurrently active users.

Scenario:
Given 0 users logged in
When a user logs in
Then there are 1 users logged in

Scenario:
Given 1 users logged in
When a user logs in
Then there are 2 users logged in

Scenario:
Given 10 users logged in
When a user logs in
Then there are 11 users logged in

Scenario:
Given 50 users logged in
When a user logs in
Then there are 51 users logged in

Scenario:
Given 99 users logged in
When a user logs in
Then there are 100 users logged in