Narrative:
The system shall distinguish between lecturers, administrators, tutors and student roles.

Scenario:
Given random log in combination
Then null will be returned

Scenario:
Given a student
When log in is correct
Then student instance will be returned

Scenario:
Given a tutor
When log in is correct
Then tutor instance will be returned

Scenario:
Given a lecturer
When log in is correct
Then lecturer instance will be returned

Scenario:
Given a administrator
When log in is correct
Then administrator instance will be returned

Scenario:
Given a student
When log in is incorrect
Then null will be returned

Scenario:
Given a tutor
When log in is incorrect
Then null will be returned

Scenario:
Given a lecturer
When log in is incorrect
Then null will be returned

Scenario:
Given a administrator
When log in is incorrect
Then null will be returned